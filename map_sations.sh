#!/bin/bash
home=/u/eu/de/haugland/data_collection


gmt set  FONT_ANNOT_PRIMARY 10
gmt set  FONT_ANNOT_SECONDARY 10
gmt set  FONT_LABEL 10
gmt set  FONT_TITLE 12
gmt set  MAP_TITLE_OFFSET -0.3c

# check if station list exists
#if [ ! -f stat_coords.dat ]; then
#    echo "creating new station coordinates file"
#    #saclst stlo stla f *BHZ*  > stat_coords.dat 
#    saclst evla evlo stla stlo f *BHZ* | awk '{print $2,$3,$4,$5}' > stat_coords.dat
#fi


region=d
proj=N180/7.5i

onefile=$(ls *BHZ* | head -n1)
dep=$(saclst evdp f $onefile | awk '{print $2}')
evdp=$(echo $dep/1000 | bc)

percent=$1
pscoast -R$region -J$proj -Bg30/g15 -Dc -A10000 -Ggray -K -P -X-0.01 > dirty_stats_map.ps
awk -v var="$percent" '{ if ($5 > var) print $4,$3}' stat_frequency.dat | gmt psxy  -B+t"$percent %" -R$region -J$proj -Si0.15c -Gblack -P -K -O >> dirty_stats_map.ps


ps2pdf12 dirty_stats_map.ps
rm *ps
evince dirty_stats_map.pdf
rm gmt.conf
rm gmt.history




