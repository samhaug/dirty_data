#!/u/eu/de/haugland/anaconda3/bin/python
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--h5_file', type=str,help='h5_file of data to be cleaned')
parser.add_argument('--dirty_list',type=str,help='list of dirty selected data')
parser.add_argument('--out_h5',type=str,help='name of output_file',default=None)

args = parser.parse_args()

import pyasdf

ds_dirty = pyasdf.ASDFDataSet(args.h5_file,mode='r')

if not args.out_h5:
    out_name = args.h5_file[:-3]+'.clean.h5'
    ds_clean = pyasdf.ASDFDataSet(out_name,mode='w')
else:
    ds_clean = pyasdf.ASDFDataSet(args.out_h5,mode='w')

f = open(args.dirty_list).readlines()[3::]

net_list  = [i.strip().split()[0] for i in f]
stat_list = [i.strip().split()[1] for i in f]

for w in ds_dirty.waveforms:
    try:
        net = w.proc_obsd[0].stats.network
        stat = w.proc_obsd[0].stats.station
        if net in net_list and stat in stat_list:
            print("Not writing %3s %5s" % (net,stat) )
            continue
        else:
            st = w.proc_obsd
            ds_clean.add_waveforms(st,'proc_obsd')
    except:
        continue
