#!/bin/bash



if [ -f net_stat ]; then
   rm net_stat
fi

for i in 2008*; do 
    sed 1,3d $i | awk '{print $1,$2,$3,$4}' >> net_stat;
done

sort net_stat | uniq > unique_stat

total_files=$(ls 2008*.dat | wc -l)
while read net stat lat lon; do
   number=$(grep "$net $stat" net_stat | wc -l)
   percent=$(echo "($number*100)/$total_files" | bc -l)
   printf "%-3s %-5s %4.2f %4.2f %4.2f\n" $net $stat $lat $lon $percent >> stat_frequency.dat
done < unique_stat
